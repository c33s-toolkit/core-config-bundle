# c33s-toolkit/core-config-bundle

Base variables to use throughout your project for Symfony 2.8|3+

## Installation

`composer require c33s-toolkit/core-config-bundle`

Add bundle to your AppKernel.php:

```php
<?php

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new C33s\Toolkit\CoreConfigBundle\C33sToolkitCoreConfigBundle(),
```

Copy `src/Resources/config/templates/c33s_toolkit_core_config.yaml` and include it
in your project config. Adapt variables to your needs.

## Usage

All settings defined in `c33s_toolkit_core_config.yaml` will be available as global twig
variables inside your templates:

```twig
<footer class="footer">
    &copy; {{ c33s_core_config.app.name }} {{ "now"|date("Y") }}
</footer>
```

This allowes for easier sharing of simple default templates or snippets between projects.
