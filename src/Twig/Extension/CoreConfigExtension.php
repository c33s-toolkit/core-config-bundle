<?php

namespace C33s\Toolkit\CoreConfigBundle\Twig\Extension;

class CoreConfigExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /**
     * @var \Twig_Environment
     */
    protected $environment;

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'c33s_core_config_extension';
    }

    /**
     * Returns a list of global variables to add to the existing list.
     *
     * @return array An array of global variables
     *
     * @deprecated since 1.23 (to be removed in 2.0), implement Twig_Extension_GlobalsInterface instead
     */
    public function getGlobals()
    {
        return array('c33s_core_config' => $this->config);
    }
}
